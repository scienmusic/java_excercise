package test;

public class test17 {
    private static void quickSorting(int[] arr) {
        quickSorting(arr, 0, arr.length - 1);
    }

    private static void quickSorting(int[] arr, int start, int end) {
        
        System.out.println("start : " + start);
        System.out.println("end : " + end);
        int part2 = partition(arr, start, end);
        System.out.println();
        System.out.println("part2 : " + part2);
        System.out.println();
        if (start < part2 - 1) {
            System.out.println("left");
            quickSorting(arr, start, part2 - 1);
        }
        if (part2 < end) {
            System.out.println("right");
            quickSorting(arr, part2, end);
        }
    }

    private static int partition(int[] arr, int start, int end) {
        int pivot = arr[(start + end) / 2];
        System.out.println("pivot : " + pivot);
        while (start <= end) {
            while (arr[start] < pivot)
                start++;
            while (pivot < arr[end])
                end--;

            if (start <= end) {
                System.out.println("arr[start]: " + arr[start]);
                System.out.println("arr[end]: " + arr[end]);
                
                if(arr[start] > arr[end] ) 
                    swap(arr, start, end);
                
                printArr(arr);
                start++;
                end--;
            }
        }
        return start;
    }
    // 4 7 3 2 5 6 1 0 9 8

    private static void swap(int[] arr, int start, int end) {
        int tmp = arr[start];
        arr[start] = arr[end];
        arr[end] = tmp;
    }

    private static void printArr(int[] arr) {
        for (int i : arr)
            System.out.print(i + " ");

        System.out.println();
    }

    public static void main(String[] args) {
        int[] arr = { 9, 7, 3, 2, 5, 6, 1, 0, 4, 8 };
        //int[] arr = {5,3,8,4,9,1,6,2,7};
        printArr(arr);
        quickSorting(arr);
//        printArr(arr);
    }

}