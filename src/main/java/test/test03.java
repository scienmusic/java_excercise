package test;

import java.util.Stack;

class MyQueue<T> {
    Stack<T> stkNew, stkOld;

    MyQueue() {
        stkNew = new Stack<T>();
        stkOld = new Stack<T>();
    }

    public int size() {
        return stkNew.size() + stkOld.size();
    }

    public void add(T value) {
        stkNew.push(value);
    }

    public void shiftStacks() {
        if (stkOld.isEmpty()) {
            while (!stkNew.isEmpty()) {
                stkOld.push(stkNew.pop());
            }
        }
    }

    public T peek() {
        shiftStacks();
        return stkOld.peek();
    }

    public T remove() {
        shiftStacks();
        return stkOld.pop();
    }
}

public class test03 {
    public static void main(String[] args) {
        MyQueue<Integer> mq = new MyQueue<Integer>();

        mq.add(1);
        mq.add(2);
        mq.add(3);

        System.out.println(mq.remove());
        System.out.println(mq.remove());
        System.out.println(mq.remove());

    }
}
