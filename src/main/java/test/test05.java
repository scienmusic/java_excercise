package test;

class Node {
    int data;
    Node next = null;

    public Node(int data) {
        this.data = data;
    }

    void append(int d) {
        Node end = new Node(d);
        Node n = this;

        while (n.next != null) {
            n = n.next;
        }
        n.next = end;
    }

    void delete(int d) {
        Node n = this;

        while (n.next != null) {
            if (n.next.data == d) {
                n.next = n.next.next;
            } else {
                n = n.next;
            }
        }

    }

    void retrieve() {
        Node n = this;
        while (n.next != null) {
            System.out.print(n.data + "?��");
            n = n.next;
        }
        System.out.println(n.data);
    }

}

public class test05 {
    public static void main(String[] args) {
        Node node = new Node(1);
        node.append(2);
        node.append(3);
        node.append(4);
        node.retrieve();
        node.delete(3);
        node.retrieve();
        node.delete(2);
        node.retrieve();
    }
}