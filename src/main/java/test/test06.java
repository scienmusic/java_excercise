package test;

class LinkedList {
    Node header;

    static class Node {
        int data;
        Node next = null;
    }

    public LinkedList() {
        header = new Node();
    }

    void append(int d) {
        Node end = new Node();
        end.data = d;
        Node n = header;

        while (n.next != null) {
            n = n.next;
        }

        n.next = end;
    }

    void delete(int d) {
        Node n = header;

        while (n.next != null) {
            if (n.next.data == d) {
                n.next = n.next.next;
            } else {
                n = n.next;
            }
        }

    }
    
    Node get(int d) {
    	Node n = header;

        while (n.next != null) {
            if (n.next.data == d) {
                return n.next;
            } else {
                n = n.next;
            }
        }
        return null;
    }

    void retrieve() { 
        Node n = header.next;
        while (n.next != null) {
            System.out.print(n.data + "? ");
            n = n.next;
        }
        System.out.println(n.data);
    }
    
    // LinkedList 중복제거 [속도 : O(n²), 공간 : O(1)]
    void removeDups() {
    	Node n = header;
    	Node r = null;
    	
    	while(n.next != null) {
    		r = n;
    		while(r.next != null) {
    			if(n.data == r.next.data) {
    				r.next = r.next.next;
    			} else {
    				r = r.next;
    			}
    		}
    		n = n.next;
    	}
    }
    
    // data 삭제와 Node 삭제는 다른 개념
    boolean deleteNode(Node n) {
    	if(n == null || n.next == null) 
    		return false;
    	
    	Node r = n.next;
    	n.data = r.data;
    	n.next = r.next;
    	
    	return true;
    }
    
    long numbering() {
    	Node n = header;
    	long result = 0;
    	long deca = 1;
    	
    	while(n.next != null) {
    		result += n.next.data * deca;
    		
    		deca *= 10;
    		n = n.next;
    	}
    	
    	return result;
    }
    
    
    //뒤부터 세기는 아직 안 했음. two pointer 기법이 제일 나음
    
    
}

public class test06 {
    public static void main(String[] args) {
        LinkedList ll = new LinkedList();
        ll.append(1);
        ll.append(2);
        ll.append(3);
        ll.append(2);
        ll.append(5);
        ll.retrieve();
        //ll.delete(3);
        //ll.retrieve();
        //ll.delete(1);
        //ll.retrieve();
        //ll.removeDups();
        ll.retrieve();
        
        System.out.println(ll.numbering());
        
        //ll.deleteNode(ll.get(2));
        ll.retrieve();
    }
}